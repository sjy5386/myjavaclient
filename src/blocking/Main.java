package blocking;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        MySocket mySocket = new MySocket("localhost", 12345);
        Thread th = new Thread(mySocket);
        th.start();
        while (true) {
            System.out.print(">>");
            String str = scanner.next();
            if (str.equalsIgnoreCase("/close"))
                break;
            mySocket.send(str);
        }
        scanner.close();
        System.exit(0);
    }
}
