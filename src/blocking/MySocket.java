package blocking;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class MySocket implements Runnable {
    private Socket socket;
    private InetAddress host;
    private BufferedReader in;
    private BufferedWriter out;

    public MySocket(String host, int port) {
        try {
            this.host = InetAddress.getByName(host);
            socket = new Socket(this.host, port);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String recv() {
        try {
            String str = in.readLine();
            return str;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void send(String str) {
        try {
            out.write(str + "\n");
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (socket.isConnected()) {
            String str = recv();
            if (str == null)
                break;
            System.out.println("Server: " + str);
        }
    }
}
